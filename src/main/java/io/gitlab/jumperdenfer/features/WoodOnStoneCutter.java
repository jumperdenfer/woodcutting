package io.gitlab.jumperdenfer.features;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Tag;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.StonecuttingRecipe;
import org.bukkit.plugin.Plugin;

/**
 * Create mutliple recipes on stoneCutter to add 
 * @author jumperdenfer
 *
 */
public class WoodOnStoneCutter {
	public List<StonecuttingRecipe> recipesList =  new ArrayList<StonecuttingRecipe>();
	private Plugin plkey;
	private FileConfiguration config;
	
	public WoodOnStoneCutter(Plugin plugin) {
		this.plkey = plugin;
		this.config = plugin.getConfig();
		this.logRecipes();
	}
	
	
	public List<StonecuttingRecipe> getRecipesList() {
		return recipesList;
	}
	
	/**
	 * Create and add a stonecutterRecipe into the List
	 * @param key
	 * @param item
	 * @param material
	 * @return 
	 */
	private void stonecutterRecipeFactory(List<ItemStack> items,Material material,String group) {
		for(ItemStack item :  items) {
			NamespacedKey key = new NamespacedKey(this.plkey,group.concat(item.getType().name()));
			StonecuttingRecipe recipe = new StonecuttingRecipe(key, item,material);
			recipe.setGroup(group);
			recipesList.add(recipe);
		}
		
		
	}

	
	/**
	 * Factorise all type of craft
	 */
	private void logRecipes() {
		for(Material log : Tag.LOGS.getValues()) {
			//Generate a groupe name 
			String group = log.name() + Math.random();
			String[] materialNameRaw = log.name().split("_");
			String firstString = materialNameRaw[0]; 
			if(firstString.contains("STRIPPED")){
				continue;
			}
			if(materialNameRaw.length > 2 && materialNameRaw[2] != null) {
				//For Dark Oak
				firstString = firstString.concat("_").concat(materialNameRaw[1]);
			}
			List<ItemStack> materialList = new ArrayList<ItemStack>();
			String[] arrayName = {
					"BUTTON",
					"FENCE",
					"FENCE_GATE",
					"PLANKS",
					"PRESSURE_PLAT",
					"SIGN",
					"SLAB",
					"STAIRS",
					"TRAPDOOR",
					"DOOR",

			};
			
			for(String item_name : arrayName ) {
				try {
					int count = this.config.getInt(item_name);
					Material materialName = Material.getMaterial(firstString.toUpperCase().concat("_").concat(item_name));
					ItemStack itemStack = new ItemStack(materialName,count);

					materialList.add(itemStack);
				}catch(Exception e) {
					continue;
				}
				
			}
			
			this.stonecutterRecipeFactory(materialList, log, group);
			
		}
		
	}
}
