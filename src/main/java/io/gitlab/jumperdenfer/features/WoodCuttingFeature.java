package io.gitlab.jumperdenfer.features;


import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

public class WoodCuttingFeature implements Listener {
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent placeEvent) {
		Block block = placeEvent.getBlock();
		World world = block.getWorld();
		Block blockBreaker = block.getRelative(BlockFace.DOWN);
		
		if(Tag.LOGS.isTagged(block.getType()) && blockBreaker.getType() == Material.STONECUTTER) {
			block.setType(Material.AIR);
			world.dropItem(block.getLocation(), this.dropPlank(block));
		}
	}
	/**
	 * Return the ItemStack containing the number of plank for the selected wood
	 * @param material
	 * @return
	 */
	private ItemStack dropPlank(Block block) {
		Material plankType = Material.OAK_PLANKS;
		switch(block.getType()) {
			case STRIPPED_ACACIA_LOG:
			case STRIPPED_ACACIA_WOOD:
			case ACACIA_LOG:
				plankType = Material.ACACIA_PLANKS;
				break;
			case STRIPPED_BIRCH_LOG:
			case STRIPPED_BIRCH_WOOD:
		 	case BIRCH_LOG:
		 		plankType = Material.BIRCH_PLANKS;
		 		break;
		 	case STRIPPED_SPRUCE_LOG:
		 	case STRIPPED_SPRUCE_WOOD:
		 	case SPRUCE_LOG:
		 		plankType = Material.SPRUCE_PLANKS;
		 		break;
		 	case STRIPPED_DARK_OAK_LOG:
		 	case STRIPPED_DARK_OAK_WOOD:
		 	case DARK_OAK_LOG:
		 		plankType = Material.DARK_OAK_PLANKS;
		 		break;
		 	case STRIPPED_JUNGLE_LOG:
		 	case STRIPPED_JUNGLE_WOOD:
		 	case JUNGLE_LOG:
		 		plankType = Material.JUNGLE_PLANKS;
		 		break;
		 	case STRIPPED_CRIMSON_STEM:
		 	case STRIPPED_CRIMSON_HYPHAE:
		 	case CRIMSON_HYPHAE:
		 	case CRIMSON_STEM:
		 		plankType=  Material.CRIMSON_PLANKS;
		 		break;
		 	case STRIPPED_WARPED_HYPHAE:
		 	case STRIPPED_WARPED_STEM:
		 	case WARPED_HYPHAE:
		 	case WARPED_STEM:
		 		plankType = Material.WARPED_PLANKS;
		 	default:
		 		break; 
		 }
		int randomCount = (int) Math.floor(Math.random() * (8 - 4 + 1) + 4);
		return new ItemStack(plankType,randomCount);
	}

}
