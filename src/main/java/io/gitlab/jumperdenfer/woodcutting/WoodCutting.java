package io.gitlab.jumperdenfer.woodcutting;


import java.io.File;
import java.util.List;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.StonecuttingRecipe;
import org.bukkit.plugin.java.JavaPlugin;
import io.gitlab.jumperdenfer.features.WoodOnStoneCutter;

public class WoodCutting extends JavaPlugin {
	
	@Override
	public void onEnable() {
		File configFile = new File(getDataFolder(),"config.yml");
		if(!configFile.exists()) {
			this.saveDefaultConfig();
		}
		WoodOnStoneCutter wostc = new WoodOnStoneCutter(this);
		List<StonecuttingRecipe> listRecipes = wostc.getRecipesList();
		for(Recipe recipe : listRecipes) {
			getServer().addRecipe(recipe);
		}
		getLogger().info("WoodCutting enable");
	}
	
	@Override
	public void onDisable() {
		getLogger().info("WoodCutting disabled");
	}
}
